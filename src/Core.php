<?php

namespace Webnestors\BibliaServices;

use stdClass;

final class Core {
	/** @var ?self */
	private static $instance = null;
	private string $basename;
	private string $root_file;
	private Settings $settings;

	private function __construct(string $root_file) {
		$this->basename = plugin_basename($root_file);
		$this->root_file = $root_file;
		$this->settings = new Settings();
	}

	/** @return self */
	static function instance(string $root_file) {
		if (!self::$instance) {
			self::$instance = new self($root_file);
		}

		return self::$instance;
	}

	function init(): void {
		if (is_admin()) {
			add_action("plugin_action_links_{$this->basename}", [$this, "action_links"]);
			add_action("admin_menu", [$this->settings, "register_menu"]);
			add_action("admin_init", [$this->settings, "register_settings"]);
		}

		add_action("init", [$this, "on_init"]);
		add_action("wp_ajax_nopriv_biblia-services:search", [$this, "ajax_action_search"]);
		add_action("wp_ajax_biblia-services:search", [$this, "ajax_action_search"]);
		// TODO: This should not be called by unauthenticated users
		add_action("wp_ajax_nopriv_biblia-services:cover", [$this, "ajax_action_cover"]);
		add_action("wp_ajax_biblia-services:cover", [$this, "ajax_action_cover"]);
		add_shortcode(
			"biblia-services:search",
			new View("search", ["biblia-services-js"], ["biblia-services-css"])
		);
	}

	function on_init(): void {
		wp_register_script(
			"biblia-services-js",
			plugins_url("/", $this->root_file) . "biblia-services.js",
			["wp-api", "jquery-form"],
			"0.1.0",
			true
		);

		wp_register_style(
			"biblia-services-css",
			plugins_url("/", $this->root_file) . "biblia-services.css",
			[],
			"0.1.0"
		);
	}

	function action_links(array $actions): array {
		$actions = array_merge(
			[
				sprintf(
					'<a href="%s">' . __("Settings") . "</a>",
					esc_url(admin_url("options-general.php?page=biblia-services"))
				)
			],
			$actions
		);

		return $actions;
	}

	function ajax_action_search(): void {
		$title_or_isbn = filter_input(\INPUT_POST, "title_or_isbn", \FILTER_SANITIZE_SPECIAL_CHARS);

		if (!$title_or_isbn || strlen($title_or_isbn) <= 2) {
			$error = new View("search-error");
			$context = new stdClass();
			$context->message = __(
				"Search must be more than 2 characters long.",
				"biblia-services"
			);
			echo $error($context);
			die();
		}

		$this->settings->preload();

		$service = new Service(
			url: $this->settings->osdelnet_endpoint_url,
			user: $this->settings->osdelnet_api_user,
			pass: $this->settings->osdelnet_api_pass
		);

		$service_response = $service->search_by_title_or_isbn($title_or_isbn);

		if (!$service_response) {
			$error = new View("search-error");
			$context = new stdClass();
			$context->message = __(
				"There was an internal service error. Please try again later.",
				"biblia-services"
			);
			echo $error($context);
			die();
		}

		$result_view = new View("result");

		if (count($service_response->docs) == 0) {
			$error = new View("search-error");
			$context = new stdClass();
			$context->message = __(
				"We couldn't find any books with your query.",
				"biblia-services"
			);
			echo $error($context);
			die();
		}

		foreach ($service_response->docs as $doc) {
			$context = new class (
				$this->settings->submit_redirect_path,
				$this->settings->jet_engine_cct_name,
				$doc
			) {
				function __construct(
					public string $redirect,
					public string $cct_name,
					public ServiceResponseDoc $doc
				) {
				}
			};

			echo $result_view($context);
		}

		die();
	}

	function ajax_action_cover(): void {
		$isbn = filter_input(\INPUT_POST, "isbn", \FILTER_SANITIZE_SPECIAL_CHARS);

		// FIXME: Some books do NOT have ISBN - what do?
		if (empty($isbn)) {
			die();
		}

		$attachments = get_posts([
			"post_type" => "attachment",
			"name" => sanitize_title($isbn),
			"posts_per_page" => 1,
			"post_status" => "inherit"
		]);

		if ($attachments) {
			/** @var \WP_Post */
			$attachment = $attachments[0];

			wp_send_json([
				"url" => wp_get_attachment_image_url($attachment->ID),
				"id" => $attachment->ID
			]);
			die();
		} else {
			// 1. Find the image URL

			$this->settings->preload();

			$service = new Service(
				url: $this->settings->osdelnet_endpoint_url,
				user: $this->settings->osdelnet_api_user,
				pass: $this->settings->osdelnet_api_pass
			);

			$service_response = $service->search_by_isbn($isbn, ["cover"]);

			if (!$service_response) {
				wp_send_json_error([
					"message" => __("Biblia Services Error (Code 1)", "biblia-services")
				]);
				die();
			}

			if (count($service_response->docs) == 0) {
				// This means that we messed up something with the search
				wp_send_json_error([
					"message" => __("Biblia Services Error (Code 2)", "biblia-services")
				]);
				die();
			}

			$img_url = $service_response->docs[0]->cover_img_url;

			if (empty($img_url)) {
				// The book does not have an image which may be normal
				wp_send_json(["id" => null]);
				die();
			}

			// 2. Download the image file

			$file_or_error = download_url(esc_url_raw($img_url));

			if (is_wp_error($file_or_error)) {
				wp_send_json_error([
					"message" => __("Biblia Services Error (Code 3)", "biblia-services")
				]);
				die();
			}

			$upload_dir = wp_upload_dir();
			$image_type = exif_imagetype($file_or_error);
			$filename = $isbn . "." . image_type_to_extension($image_type);
			$absolute_filename = trailingslashit($upload_dir["path"]) . $filename;
			copy($file_or_error, $absolute_filename);
			@unlink($file_or_error);

			// 3. Create the WordPress media image post type

			$attach_id = wp_insert_attachment(
				[
					"guid" => trailingslashit($upload_dir["url"]) . $filename,
					"post_mime_type" => image_type_to_mime_type($image_type),
					"post_title" => sanitize_title($isbn),
					"post_content" => "",
					"post_status" => "inherit"
				],
				$absolute_filename,
				0
			);

			if (!$attach_id) {
				wp_send_json_error([
					"message" => __("Biblia Services Error (Code 4)", "biblia-services")
				]);
				die();
			}

			$attach_data = wp_generate_attachment_metadata($attach_id, $absolute_filename);
			wp_update_attachment_metadata($attach_id, $attach_data);

			wp_send_json(["id" => $attach_id]);
			die();
		}
	}
}
