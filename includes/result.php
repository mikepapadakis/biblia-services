<div class="biblia-services-result">
	<div class="biblia-services-result-cover">
		<?php if (!empty($doc->cover_img_url)): ?>
			<img src="<?php echo wp_kses_post($doc->cover_img_url); ?>" />
		<?php endif; ?>
	</div>

	<div class="biblia-services-result-main">
		<div>
			<div class="book-title">
				<?php echo wp_kses_post($doc->distinctive_title); ?>
			</div>

			<?php if (!empty($doc->distinctive_subtitle)): ?>
				<div class="book-distinctive-title">
					<?php echo $doc->distinctive_subtitle; ?>
				</div>
			<?php endif; ?>
		</div>

		<div>
			<?php if (!empty($doc->original_title)): ?>
				<div class="book-original-title">
					<?php echo $doc->original_title; ?>
				</div>
			<?php endif; ?>

			<?php if (!empty($doc->original_subtitle)): ?>
				<div class="book-original-subtitle">
					<?php echo $doc->original_subtitle; ?>
				</div>
			<?php endif; ?>

			<?php if (!empty($doc->isbn13)): ?>
				<div class="book-isbn">
					<?php echo $doc->isbn13; ?>
				</div>
			<?php endif; ?>

			<?php if (!empty($doc->pub_name)): ?>
				<div class="book-pub-name">
					<?php echo $doc->pub_name; ?>
				</div>
			<?php endif; ?>

			<?php if (!empty($doc->pub_year)): ?>
				<div class="book-pub-year">
					<?php echo $doc->pub_year; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<form
		method="POST"
		class="biblia-services-submit-form"
	>
		<input type="hidden" name="ajax_url" value="<?php echo admin_url("admin-ajax.php"); ?>" />
		<input type="hidden" name="redirect" value="<?php echo esc_attr($redirect); ?>" />
		<input type="hidden" name="cct_name" value="<?php echo esc_attr($cct_name); ?>" />
		<input type="hidden" name="book_title" value="<?php echo esc_attr($doc->distinctive_title); ?>" />
		<input type="hidden" name="isbn_13" value="<?php echo esc_attr($doc->isbn13); ?>" />
		<input
			type="hidden"
			name="book_description"
			value="<?php echo esc_attr($doc->description ?? "N/A"); ?>"
		/>
		<input
			type="hidden"
			name="published_year"
			value="<?php echo esc_attr($doc->pub_year ?? "N/A"); ?>"
		/>
		<input
			type="hidden"
			name="bookstore_price"
			value="<?php echo esc_attr($doc->price ?? "N/A"); ?>"
		/>
		<input
			type="hidden"
			name="book_pages"
			value="<?php echo esc_attr($doc->pages ?? "N/A"); ?>"
		/>
		<input
			type="hidden"
			name="publication_by"
			value="<?php echo esc_attr($doc->pub_name ?? "N/A"); ?>"
		/>
		<button class="biblia-services-button" type="submit">
			<span class="button-content"><?php _e("Select"); ?></span>
			<span class="button-spinner"></span>
		</button>
	</form>
</div>
