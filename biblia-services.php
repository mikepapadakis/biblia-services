<?php

/*
 * Plugin Name: Biblia Services
 * Description: Biblia Services
 * Version: 0.1.0
 * Requires PHP: 8.1
 * Author: Nikos Papadakis, WebNestors
 * Author URI: https://webnestors.com
 * Text Domain: biblia-services
 * Domain Path: /languages
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 */

if (!defined("WPINC")) {
	die();
}

require plugin_dir_path(__FILE__) . "vendor/autoload.php";

Webnestors\BibliaServices\Core::instance(__FILE__)->init();
