{
  description = "Flake for biblia_services";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.simpleFlake {
    inherit self nixpkgs;
    name = "biblia_services";
    shell = { pkgs }: with pkgs; mkShell {
      packages = [ nodejs_20 php81Packages.psalm nodePackages.typescript-language-server ];
    };
  };
}
