function toggleLoadingButton(target) {
  target.prop("disabled", target.prop("disabled") ? false : true);
  jQuery(target).find(".button-content").toggle();
  jQuery(target).find(".button-spinner").toggle();
}

function attachSubmitHandlers() {
  jQuery(".biblia-services-submit-form").on("submit", function(e) {
    e.preventDefault();

    const data = Object.fromEntries(new FormData(e.target));
    const submitButton = jQuery(e.target).find("button[type=submit]");

    toggleLoadingButton(submitButton);

    // Create or get the cover image in the media gallery
    // TODO: User should be logged in at this point, redirect them to login page if not.
    jQuery.post(data.ajax_url, {
      action: "biblia-services:cover",
      isbn: data.isbn_13,
    }).done(function(coverRes) {
      if (coverRes.success === false) {
        // TODO: Show an error message to user
        console.error(coverRes.message || "Internal server error");
        return;
      }

      wp.apiRequest({
        path: `/jet-cct/${data.cct_name}`,
        type: "POST",
        data: {
          book_title: data.book_title,
          isbn_13: data.isbn_13,
          book_description: data.book_description,
          published_year: data.published_year,
          bookstore_price: data.bookstore_price,
          book_pages: data.book_pages,
          publication_by: data.publication_by,
          book_cover: coverRes.id || undefined,
        },
      }).done(function(createRes) {
        if (createRes.success === true) {
          const path = data.redirect + `?item_id=${createRes.item_id}`;
          window.location.replace(path);
        } // TODO: Handle error
      }).fail(function() {
        // TODO: Auth error: 401
        // TODO: Route Not Found error: 404
      }).always(function() {
        toggleLoadingButton(submitButton);
      });
    }).fail(function() {
      // TODO: Show an error message to user
      toggleLoadingButton(submitButton);
    });
  });
}

document.addEventListener("DOMContentLoaded", function() {
  if (!wp.apiRequest) {
    console.error("biblia-services: missing required dependency `wp.apiRequest`");
    return;
  }

  const submitButton = jQuery("#biblia-services-search-form button[type=submit]");
  submitButton.find(".button-spinner").hide();

  jQuery("#biblia-services-search-form").ajaxForm({
    target: "#biblia-services-results",

    success: function(data) {
      if (data.indexOf("search-error") !== -1) {
        jQuery("#biblia-services-search-form .search-input").addClass("error");
      } else {
        jQuery("#biblia-services-search-form .search-input").removeClass("error");
      }

      jQuery(".biblia-services-submit-form .button-spinner").hide();
      attachSubmitHandlers();
    },

    error: function() {
      alert("an error occured 3");
    },

    beforeSend: function() {
      toggleLoadingButton(submitButton);
    },

    complete: function() {
      toggleLoadingButton(submitButton);
    },
  });
});
